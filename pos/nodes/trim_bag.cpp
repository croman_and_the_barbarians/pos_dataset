#include <stdio.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>


int main(int argc, char* argv[])
{

  boost::filesystem::path dir(argv[2]);
  if(boost::filesystem::create_directory(dir.parent_path()))
  {
      std::cerr<< "Directory Created: "<<dir.parent_path()<<std::endl;
  }

  rosbag::Bag bag;
  bag.open(argv[1]);  // BagMode is Read by default
  rosbag::Bag out_bag;
  out_bag.open(argv[2],rosbag::bagmode::Write);



  rosbag::View view(bag);
  size_t bag_len = view.size();
  size_t i = 0;

  ros::Duration time_offset;
  time_offset.fromSec(10.0);

  ros::Time start_time;
  start_time = view.getBeginTime() + time_offset;

  for(rosbag::MessageInstance const m: view)
  {
    if (i % 100000 == 0){
      printf ("\rpercent complete: %.2f", double(i)/double(bag_len)*100.0);
      fflush(stdout);
    }
    i++;
    if(m.getTopic()!="/clock" && m.getTime() > start_time){
      out_bag.write(m.getTopic(), m.getTime(), m, m.getConnectionHeader());
    }else if(m.getTopic()=="/tf_static"){
      out_bag.write(m.getTopic(), start_time, m, m.getConnectionHeader());
    }
  }

  printf ("\rpercent complete: %.2f \n", double(i)/double(bag_len)*100.0);

  bag.close();
  out_bag.close();

}
