#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 11:02:14 2020

@author: kris
"""

import rosbag
import threading, Queue
import os
import sys

from rospy_message_converter import json_message_converter
from norbit_msgs.msg import Bathymetric

myQueue = Queue.Queue()


def processTopic(current_topic,bag,outdir):
    print(current_topic)
    dirname=outdir+current_topic
    head, tail = os.path.split(dirname)
    if not os.path.exists(head):
        os.makedirs(head)
    f = open(dirname+".json", "w")
    
    for topic, msg, t in bag.read_messages(topics=current_topic):
        try:
            json_str = json_message_converter.convert_ros_message_to_json(msg)
            f.write(json_str)
            f.write('\n')

        except:
            print("can't process", topic)
            break
            
#def doWork():
#    # As long as their is work to be done
#    while True:
#        processTopic(myQueue.get())
#       # print("processing" myQueue.get());
#        myQueue.task_done()

def main():
    str(sys.argv)
    bag = rosbag.Bag(str(sys.argv[1]))
    outdir = str(sys.argv[2])

    all_topics = bag.get_type_and_topic_info()[1].keys()
    for item in all_topics:
        if not "detections" in item:
          processTopic(item,bag,outdir)

    bag.close()


if __name__ == "__main__":
    main()
