#ifndef CTD_WIDGET_H
#define CTD_WIDGET_H

#include <QWidget>
#include "topic_widget.h"
#include "ds_sensor_msgs/Ctd.h"

namespace Ui {
class CtdWidget;
}

class CtdWidget : public TopicWidget
{
    Q_OBJECT

public:
    explicit CtdWidget(QWidget *parent = nullptr);
    ~CtdWidget();
    void setup(std::string topic, ros::NodeHandlePtr node);
    void ctdCallback(const ds_sensor_msgs::Ctd::ConstPtr& msg);


//protected slots:
//    void msgTimeout();

private:
    Ui::CtdWidget *ui;
};

#endif // CTD_WIDGET_H
