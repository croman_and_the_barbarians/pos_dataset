#include "airmax_widget.h"
#include "ui_airmax_widget.h"

AirmaxWidget::AirmaxWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AirmaxWidget)
{
    ui->setupUi(this);

    airmaxTimeout();

    ui_timer_ = new QTimer(this);
    connect(ui_timer_, SIGNAL(timeout()), this, SLOT(airmaxTimeout()));
}

AirmaxWidget::~AirmaxWidget()
{
    delete ui;
}

void AirmaxWidget::setup(std::string topic, ros::NodeHandlePtr node){
    ui->airmax_station_lbl->setText(QString::fromStdString(topic));
    node_ptr_=node;
    sub_ = node_ptr_->subscribe<pos_msgs::AirmaxWirelessStamped>(topic,1,&AirmaxWidget::airmaxCallback,this);
}

void AirmaxWidget::airmaxCallback(const pos_msgs::AirmaxWirelessStamped::ConstPtr &airmaxMsg){
    this->setEnabled(true);
    ui->signal_strength->setValue(airmaxMsg->wireless.signal);

    ui_timer_->start(5000);
}

void AirmaxWidget::airmaxTimeout(){
    this->setEnabled(false);
}
