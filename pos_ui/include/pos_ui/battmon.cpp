#include "battmon.h"
#include "ui_battmon.h"

BattMon::BattMon(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BattMon)
{
    ui->setupUi(this);

    ui_timer_ = new QTimer(this);
    connect(ui_timer_, SIGNAL(timeout()), this, SLOT(msgTimeout()));


    this->setEnabled(false);
}

BattMon::~BattMon()
{
    delete ui;
}


void BattMon::setup(std::string topic, ros::NodeHandlePtr node){
    ui->topic_lbl->setText(QString::fromStdString(topic));
    node_ptr_=node;
    sub_ = node_ptr_->subscribe<sensor_msgs::BatteryState>(topic,1,&BattMon::batteryCallback,this);
}

void BattMon::batteryCallback(const sensor_msgs::BatteryState::ConstPtr &msg){
    this->setEnabled(true);

    ui->bat_percent->setValue(msg->percentage*100.0);
    ui->voltage_number->display(msg->voltage);
    ui_timer_->start(5000);
}


void BattMon::msgTimeout(){
    this->setEnabled(false);
}
