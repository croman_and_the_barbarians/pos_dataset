#ifndef CONFIGURE_SURVEY_H
#define CONFIGURE_SURVEY_H

#include <QDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include "yaml-cpp/yaml.h"
#include <fstream>
namespace Ui {
class ConfigureSurvey;
}

class ConfigureSurvey : public QDialog
{
  Q_OBJECT

public:
  explicit ConfigureSurvey(QWidget *parent = 0);
  ~ConfigureSurvey();

    sensor_msgs::NavSatFix currentFix;
    sensor_msgs::NavSatFixPtr getCurrentFix();
    void enableInpuut(bool x);
    void updateParams();
    void sendParams();
    void refreshTopics();

    void gpsCallback(const ros::MessageEvent<const sensor_msgs::NavSatFix> &event);

private slots:
    void on_useCurrentLocationBtn_clicked();

    void on_browseBtn_clicked();

    void on_buttonBox_accepted();

    void on_loadCfgBtn_clicked();

    void on_cfgFileInput_textChanged(const QString &arg1);

    void on_buttonBox_rejected();

    void on_refresh_btn_clicked();

private:
  Ui::ConfigureSurvey *ui;
  ros::NodeHandle _node;
  std::map<std::string,std::shared_ptr<ros::Subscriber>> gps_subs_;
  std::map<std::string,sensor_msgs::NavSatFixPtr> gps_msgs_;

};

#endif // CONFIGURE_SURVEY_H
