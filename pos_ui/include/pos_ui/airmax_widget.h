#ifndef AIRMAX_WIDGET_H
#define AIRMAX_WIDGET_H

#include <QWidget>
#include <ros/ros.h>
#include <pos_msgs/AirmaxWirelessStamped.h>
#include <QTimer>

namespace Ui {
class AirmaxWidget;
}

class AirmaxWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AirmaxWidget(QWidget *parent = nullptr);
    ~AirmaxWidget();
    void setup(std::string lbl, ros::NodeHandlePtr node);
    void airmaxCallback(const pos_msgs::AirmaxWirelessStamped::ConstPtr& airmaxMsg);


private:
    Ui::AirmaxWidget *ui;
    ros::NodeHandlePtr node_ptr_;
    ros::Subscriber sub_;
    QTimer *ui_timer_;

private slots:
    void airmaxTimeout();

};

#endif // AIRMAX_WIDGET_H
