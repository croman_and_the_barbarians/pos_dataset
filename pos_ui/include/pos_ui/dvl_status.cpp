#include "dvl_status.h"
#include "ui_dvl_status.h"

DVLStatus::DVLStatus(QWidget *parent) :
  TopicWidget(parent),
  ui(new Ui::DVLStatus)
{
  ui->setupUi(this);
}

DVLStatus::~DVLStatus()
{
  delete ui;
}

void DVLStatus::setup(std::string topic, ros::NodeHandlePtr node){
  node_ptr_=node;
  sub_ = node_ptr_->subscribe<ds_sensor_msgs::Dvl>(topic,1,&DVLStatus::callback,this);
  ui->label->setText(QString::fromStdString(topic));
}

void DVLStatus::callback(const ds_sensor_msgs::Dvl::ConstPtr& msg){
  double avg = 0.0;
  double min = INFINITY;
  for(size_t i = 0; i<4 ; i++){
    double alt = msg->range[i] * sqrt(3)/2;
    avg+=alt;
    min = std::min(min, alt);
  }
  avg/=4;
  ;
  ui->min_depth->setText(QString::number(min,'f', 2));
  ui->avg_depth->setText(QString::number(avg,'f', 2));
  resetTimer();
}
