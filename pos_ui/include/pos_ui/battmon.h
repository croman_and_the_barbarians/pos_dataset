#ifndef BATTMON_H
#define BATTMON_H

#include <QWidget>
#include <QTimer>
#include <ros/ros.h>
#include <sensor_msgs/BatteryState.h>

namespace Ui {
class BattMon;
}

class BattMon : public QWidget
{
    Q_OBJECT

public:
    explicit BattMon(QWidget *parent = nullptr);
    ~BattMon();
    void setup(std::string topic, ros::NodeHandlePtr node);
    void batteryCallback(const sensor_msgs::BatteryState::ConstPtr& msg);

private slots:
    void msgTimeout();

private:
    Ui::BattMon *ui;
    ros::NodeHandlePtr node_ptr_;
    ros::Subscriber sub_;
    QTimer *ui_timer_;
};

#endif // BATTMON_H
