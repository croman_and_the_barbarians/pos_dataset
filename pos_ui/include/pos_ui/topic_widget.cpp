#include "topic_widget.h"

TopicWidget::TopicWidget(QWidget *parent) :
    QWidget(parent)
{
    ui_timer_ = new QTimer(this);
    connect(ui_timer_, SIGNAL(timeout()), this, SLOT(msgTimeout()));
    timeout_ = 5000;
    ui_timer_->start(timeout_);
    msgTimeout(); // start in a time out stat
}

TopicWidget::~TopicWidget()
{
    return;
}


void TopicWidget::setup(std::string topic, ros::NodeHandlePtr node){
    return;
}


void TopicWidget::msgTimeout(){
  this->setEnabled(false);
}


void TopicWidget::resetTimer(){
  this->setEnabled(true);
  ui_timer_->start(timeout_);
}
