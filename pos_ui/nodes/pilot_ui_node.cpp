#include <../include/pos_ui/pilot_ui.h>
#include <QApplication>
#include <QtCore/qglobal.h>
#include <ros/ros.h>
#include <../include/pos_ui/configure_survey.h>

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "pilot_gui_node");

    QApplication a(argc, argv);
    PilotUI w;
    w.show();


//    ConfigureSurvey w;
//    w.show();

    return a.exec();
}
