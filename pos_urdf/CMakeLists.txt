cmake_minimum_required(VERSION 2.8.3)

project(pos_urdf)

find_package(catkin REQUIRED)

catkin_package()

find_package(roslaunch)


find_package(xacro REQUIRED)
xacro_add_files(${CMAKE_CURRENT_SOURCE_DIR}/urdf/pos_2020.urdf.xacro TARGET pos_2020)
install(FILES
    urdf/pos_2020.urdf
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})


foreach(dir config launch meshes urdf)
  install(DIRECTORY ${dir}/
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/${dir})
endforeach(dir)
